package com.gmail.borlandlp.features.mainpage.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.gmail.borlandlp.features.R
import com.gmail.borlandlp.features.mainpage.model.Good

class DetailedGoodView {
    var listener: Listener? = null

    @Composable
    @Preview(showBackground = true)
    fun GetView(
        @PreviewParameter(DetailedPromoGoodsView.FakeDetailedGoodsProvider::class) good: Good
    ) {
        ConstraintLayout(
            modifier = Modifier
                .width(262.dp)
                .height(314.dp)
                .clip(shape = RoundedCornerShape(8.dp))
                .background(color = colorResource(id = R.color.white))
                .clickable {
                    listener?.onGoodClicked(good = good)
                }
        ) {
            val (categoryTitle, name, price, favoriteBtn, cartBtn, photo, backgroundView) = createRefs()
            /** Что-бы не дублировать паддинг и индекс */
            val itemsModifier: Modifier = Modifier
                .zIndex(1F)
                .padding(12.dp)

            Text(
                text = "Top of the month",
                fontSize = 15.sp,
                modifier = itemsModifier
                    .constrainAs(categoryTitle) {
                        top.linkTo(parent.top)
                        start.linkTo(parent.start)
                    }
            )

            Text(
                text = good.name,
                fontWeight = FontWeight.Bold,
                fontSize = 16.sp,
                modifier = itemsModifier
                    .constrainAs(name) {
                        top.linkTo(categoryTitle.bottom)
                        start.linkTo(categoryTitle.start)
                    }
            )

            Text(
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp,
                text = "\$${good.price}",
                modifier = itemsModifier
                    .constrainAs(price) {
                        bottom.linkTo(parent.bottom)
                        start.linkTo(parent.start)
                    }
                    .padding(bottom = 10.dp)
            )

            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(good.imageUrl)
                    .crossfade(true)
                    .build(),
                placeholder = painterResource(R.drawable.image_placeholder),
                contentDescription = stringResource(R.string.photo_of_good),
                contentScale = ContentScale.Fit,
                modifier = itemsModifier
                    .clip(RoundedCornerShape(0))
                    .constrainAs(photo) {
                        top.linkTo(name.bottom)
                        bottom.linkTo(cartBtn.top)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        height = Dimension.fillToConstraints
                        width = Dimension.fillToConstraints
                    }
                    .padding(20.dp)

            )

            Image(
                painter = painterResource(id = R.drawable.ic_heart_filled),
                contentDescription = "favorite btn",
                modifier = itemsModifier
                    .constrainAs(favoriteBtn) {
                        top.linkTo(categoryTitle.top)
                        end.linkTo(parent.end)
                    }
                    .padding(top = 4.dp)
                    .width(18.dp)
                    .clickable {
                        val isInFavorites = true
                        listener?.onFavoriteStateChange(good = good, isInFavorites)
                        good.inFavorites = isInFavorites
                    }
            )

            Image(
                painter = painterResource(id = R.drawable.ic_cart_add),
                contentDescription = "cart btn",
                modifier = itemsModifier
                    .constrainAs(cartBtn) {
                        end.linkTo(parent.end)
                        top.linkTo(price.top)
                        bottom.linkTo(price.bottom)
                    }
                    .width(40.dp)
                    .clickable {
                        val isInCart = true
                        listener?.onCartStateChange(good = good, isInCart)
                        good.inCart = isInCart
                    }
            )

            Box(
                modifier = Modifier
                    .background(
                        colorResource(R.color.blue),
                        shape = RoundedCornerShape(
                            topStart = 100.dp,
                            topEnd = 10.dp
                        )
                    )
                    .fillMaxWidth()
                    .height(230.dp)
                    .constrainAs(backgroundView) {
                        top.linkTo(name.bottom)
                        bottom.linkTo(parent.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
            )
        }
    }

    interface Listener {
        fun onFavoriteStateChange(good: Good, newState: Boolean)
        fun onCartStateChange(good: Good, newState: Boolean)
        fun onGoodClicked(good: Good)
    }
}