package com.gmail.borlandlp.features.mainpage.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.gmail.borlandlp.features.R

class SearchFieldView {
    var listener: Listener? = null

    @Composable
    @Preview(showBackground = true)
    fun GetView() {
        ConstraintLayout(
            modifier = Modifier
                .fillMaxWidth()
                .background(color = colorResource(id = R.color.gray_background))
                .padding(16.dp)
        ) {
            val (searchField, filterBtn) = createRefs()

            TextField(
                value = "",
                placeholder = { Text(text = "Search here...") },
                onValueChange = {

                },
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = colorResource(id = R.color.white),
                    cursorColor = Color.Black,
                    disabledLabelColor = Color.Transparent,
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                    placeholderColor = Color.Gray
                ),
                modifier = Modifier
                    .constrainAs(searchField) {
                        start.linkTo(parent.start)
                        end.linkTo(filterBtn.start)
                        width = Dimension.fillToConstraints
                    }
                    .clip(shape = RoundedCornerShape(8.dp))
                    .background(color = colorResource(id = R.color.super_white))
                    .height(57.dp)
            )

            Image(
                painter = painterResource(id = R.drawable.ic_filter),
                contentDescription = "filter button",
                modifier = Modifier
                    .constrainAs(filterBtn) {
                        end.linkTo(parent.end)
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                    }
                    .padding(start = 16.dp)
                    .width(58.dp)
                    .clip(shape = RoundedCornerShape(8.dp))
                    .background(color = colorResource(id = R.color.white))
                    .padding(start = 5.dp, end = 5.dp, top = 19.dp, bottom = 19.dp)
            )
        }
    }

    interface Listener {
        fun onFiltersClicked()
        fun onSearch(query: String)
    }
}