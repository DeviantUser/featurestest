package com.gmail.borlandlp.features.mainpage.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import com.gmail.borlandlp.features.R
import com.gmail.borlandlp.features.mainpage.model.Good

class GoodsWithRubricCollectionView {
    var listener: Listener? = null

    @Composable
    @Preview(showBackground = true)
    fun GetView(
        @PreviewParameter(FakePromoGoodsPageDataProvider::class) pageData: PromoGoodsPageData
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            PromoGoodsHeaderView()

            DetailedPromoGoodsView().apply {
                listener = object : DetailedGoodView.Listener {
                    override fun onFavoriteStateChange(good: Good, newState: Boolean) {
                        this@GoodsWithRubricCollectionView.listener?.onFavoriteStateChange(good, newState)
                    }

                    override fun onCartStateChange(good: Good, newState: Boolean) {
                        this@GoodsWithRubricCollectionView.listener?.onCartStateChange(good, newState)
                    }

                    override fun onGoodClicked(good: Good) {
                        this@GoodsWithRubricCollectionView.listener?.onGoodClicked(good)
                    }

                }
                GetView(pageData.detailedGoods)
            }
        }
    }

    @Composable
    @Preview(showBackground = true)
    private fun PromoGoodsHeaderView() {
        ConstraintLayout(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            val (titleView, dotsButton) = createRefs()

            Text(
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold,
                text = "Popular",
                modifier = Modifier
                    .constrainAs(titleView) {
                        top.linkTo(parent.top)
                        start.linkTo(parent.start)
                    }
            )
            Image(
                painter = painterResource(id = R.drawable.ic_dots),
                contentDescription = "menu",
                modifier = Modifier
                    .constrainAs(dotsButton) {
                        end.linkTo(parent.end)
                        top.linkTo(titleView.top)
                        bottom.linkTo(titleView.bottom)
                    }
                    .width(45.dp)
                    .clickable {
                        listener?.onRubricChange()
                    }
            )
        }
    }

    data class PromoGoodsPageData (
        val detailedGoods: List<Good>
    )

    interface Listener {
        fun onRubricChange()
        fun onFavoriteStateChange(good: Good, newState: Boolean)
        fun onCartStateChange(good: Good, newState: Boolean)
        fun onGoodClicked(good: Good)
    }
}

class FakePromoGoodsPageDataProvider(
    override val values: Sequence<GoodsWithRubricCollectionView.PromoGoodsPageData> = listOf(
        GoodsWithRubricCollectionView.PromoGoodsPageData(
            detailedGoods = DetailedPromoGoodsView.FakeDetailedGoodsProvider().values.toList(),
        )
    ).asSequence()
) : PreviewParameterProvider<GoodsWithRubricCollectionView.PromoGoodsPageData>