package com.gmail.borlandlp.features.mainpage.view

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.gmail.borlandlp.features.R
import com.gmail.borlandlp.features.ui.theme.FeaturesTestTheme


/**
 * 1) Бок скидок - при вводе текста блок поплуряного скрывается и отображается шиммер эффект, после
 * Отображается поисковая выдача. При выборе фильтра эффект такой же
 * 2) Блок популярного - при клике на три точки отображается попап с видами одежды
 * При выборе типа одежды - блок популярного актуализируется в соответствии с типом одежды
 * (Подгружается заново, с шиммер эффектом
 * 3) При свайпе детальных вью вправо/влево анимации закраски фона?
*/

class MainScreen {

    var testCallback1: () -> Unit = {}

    @Preview(showBackground = true)
    @Composable
    fun DefaultPreview() {
        FeaturesTestTheme {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(colorResource(id = R.color.gray_background))
                    .verticalScroll(rememberScrollState())
            ) {
                TopBarView().apply {
                    GetView()
                }

                SearchFieldView().apply {
                    GetView()
                }

                GoodsWithRubricCollectionView().apply {
                    GetView(
                        GoodsWithRubricCollectionView.PromoGoodsPageData(
                            DetailedPromoGoodsView.FakeDetailedGoodsProvider().values.toList()
                        )
                    )
                }

                Spacer(modifier = Modifier.height(10.dp))

                GoodsCollectionSmallView().apply {
                    GetView(smallGoods = DetailedPromoGoodsView.FakeDetailedGoodsProvider().values.toList())
                }
            }
        }
    }
}