package com.gmail.borlandlp.features

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.ui.Modifier
import com.gmail.borlandlp.features.mainpage.view.MainScreen
import com.gmail.borlandlp.features.ui.theme.FeaturesTestTheme

/**
 * Цели приложения:
 * 1) Освоить compose вёрстку
 * 2) получить опыт внедрения MVP
 * 3) Получить опыт работы с бэком
 * 4) Эксперименты по навигации?
 * 5) Dagger2?
 */
// https://figma.community/files/file/954-shablon-nike-shoes-app-dlya-figma/
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
    }

    private fun initUI() {
        setContent {
            FeaturesTestTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MainScreen().apply {
                        DefaultPreview()
                        testCallback1 = {
                            Toast.makeText(baseContext, "TestCall", Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
        }
    }
}