package com.gmail.borlandlp.features.mainpage.view

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.gmail.borlandlp.features.R
import com.gmail.borlandlp.features.mainpage.model.Good

class GoodsCollectionSmallView {
    var listener: GoodPreview.Listener? = null

    @Composable
    @Preview(showBackground = true)
    fun GetView(
        @PreviewParameter(DetailedPromoGoodsView.FakeDetailedGoodsProvider::class) smallGoods: List<Good>
    ) {
        ConstraintLayout(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp, start = 16.dp, end = 0.dp, bottom = 16.dp)
                .clip(shape = RoundedCornerShape(8.dp))
                .background(colorResource(id = R.color.dark_gray))
                .padding(end = 16.dp, top = 16.dp, bottom = 16.dp)

        ) {
            val (categoryName, goodsListView) = createRefs()

            Text(
                fontWeight = FontWeight.Bold,
                fontSize = 14.sp,
                text = "New items",
                modifier = Modifier
                    .constrainAs(categoryName) {
                        top.linkTo(parent.top)
                        start.linkTo(parent.start)
                    }
                    .padding(start = 0.dp, top = 60.dp, end = 0.dp, bottom = 0.dp)
                    .height(20.dp)
                    .rotate(270F)
                    .padding(start = 0.dp, top = 0.dp, end = 0.dp, bottom = 0.dp)
            )

            Column(
                content = {
                    smallGoods.forEach { good ->
                        GoodPreview().apply {
                            this.listener = this@GoodsCollectionSmallView.listener
                            GetView(good = good)
                        }
                        if (smallGoods.indexOfFirst { it.id == good.id } < smallGoods.size-1) {
                            Spacer(
                                modifier = Modifier.height(10.dp)
                            )
                        }
                    }
                },
                modifier = Modifier
                    .constrainAs(goodsListView) {
                        start.linkTo(categoryName.end)
                        top.linkTo(parent.top)
                        end.linkTo(parent.end)
                        width = Dimension.fillToConstraints
                    }
            )
        }
    }
}