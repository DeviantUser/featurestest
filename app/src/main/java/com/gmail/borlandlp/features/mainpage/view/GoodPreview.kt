package com.gmail.borlandlp.features.mainpage.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.gmail.borlandlp.features.R
import com.gmail.borlandlp.features.mainpage.model.Good

class GoodPreview {
    var listener: Listener? = null

    @Composable
    @Preview(showBackground = true)
    fun GetView(
        @PreviewParameter(DetailedPromoGoodsView.FakeDetailedGoodsProvider::class) good: Good
    ) {
        ConstraintLayout(
            modifier = Modifier
                .fillMaxWidth()
                .height(102.dp)
                .clip(shape = RoundedCornerShape(8.dp))
                .background(colorResource(id = R.color.white))
                .padding(10.dp)

        ) {
            val (photoView, nameView, priceView, favoriteBtn, cartBtn) = createRefs()

            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(good.imageUrl)
                    .crossfade(true)
                    .build(),
                placeholder = painterResource(R.drawable.image_placeholder),
                contentDescription = stringResource(R.string.photo_of_good),
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .constrainAs(photoView) {
                        top.linkTo(parent.top)
                        start.linkTo(parent.start)
                        bottom.linkTo(parent.bottom)
                        height = Dimension.fillToConstraints
                    }
                    .width(110.dp)
            )

            Text(
                text = good.name,
                modifier = Modifier
                    .constrainAs(nameView) {
                        top.linkTo(parent.top)
                        start.linkTo(photoView.end)
                    }
                    .padding(start = 10.dp)
            )

            Text(
                fontWeight = FontWeight.Bold,
                text = "\$${good.price}",
                modifier = Modifier
                    .constrainAs(priceView) {
                        bottom.linkTo(parent.bottom)
                        start.linkTo(photoView.end)
                    }
                    .padding(start = 10.dp)
            )

            Image(
                painter = painterResource(id = R.drawable.ic_heart_filled),
                contentDescription = "favorite button",
                modifier = Modifier
                    .constrainAs(favoriteBtn) {
                        top.linkTo(parent.top)
                        end.linkTo(parent.end)
                    }
                    .width(18.dp)
            )

            Image(
                painter = painterResource(id = R.drawable.ic_cart_add),
                contentDescription = "add to cart button",
                modifier = Modifier
                    .constrainAs(cartBtn) {
                        bottom.linkTo(parent.bottom)
                        end.linkTo(parent.end)
                    }
                    .width(18.dp)
            )
        }
    }

    interface Listener {
        fun onFavoriteStateChange(good: Good, newState: Boolean)
        fun onCartStateChange(good: Good, newState: Boolean)
        fun onGoodClicked(good: Good)
    }
}