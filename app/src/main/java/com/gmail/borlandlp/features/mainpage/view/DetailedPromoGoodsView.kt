package com.gmail.borlandlp.features.mainpage.view

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCompositionContext
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.gmail.borlandlp.features.R
import com.gmail.borlandlp.features.mainpage.model.Good

class DetailedPromoGoodsView {

    var listener: DetailedGoodView.Listener? = null

    @Composable
    @Preview(showBackground = true)
    fun GetView(
        @PreviewParameter(FakeDetailedGoodsProvider::class) goods: List<Good>
    ) {
        LazyRow(content = {
            items(goods) { good ->
                if (goods.indexOfFirst { it.id == good.id } == 0) {
                    Spacer(
                        modifier = Modifier.width(16.dp)
                    )
                }
                DetailedGoodView().apply {
                    this.listener = this@DetailedPromoGoodsView.listener
                    GetView(good = good)
                }
                Spacer(
                    modifier = Modifier.width(16.dp)
                )
            }
        })
    }

    class FakeDetailedGoodsProvider(
        override val values: Sequence<Good> = listOf(
            Good(
                id = "1",
                name = "Air Max 270 G",
                price = 55.50F,
                inCart = false,
                inFavorites = false,
                imageUrl = "https://krasnoyarsk.kedred.ru/wp-content/uploads/2019/03/Nike-Air-Force-1-07-Mid-Utility-RedBlack-4790-41-45-1-e1553099343729.png"
            ),
            Good(
                id = "2",
                name = "Nike Power",
                price = 55.50F,
                inCart = true,
                inFavorites = true,
                imageUrl = "https://e7.pngegg.com/pngimages/378/372/png-clipart-nike-air-max-sports-shoes-nike-free-4-v4-men-s-shoes-size-7-size-7-nike.png"
            ),
            Good(
                id = "3",
                name = "Sample good3",
                price = 55.50F,
                inCart = true,
                inFavorites = false,
                imageUrl = "https://e7.pngegg.com/pngimages/820/94/png-clipart-shoe-nike-air-max-sneakers-running-running-shoes-orange-outdoor-shoe.png"
            ),
            Good(
                id = "4",
                name = "Sample good4",
                price = 55.50F,
                inCart = false,
                inFavorites = true,
                imageUrl = "https://e7.pngegg.com/pngimages/753/95/png-clipart-cleveland-cavaliers-nike-sports-shoes-adidas-cleveland-cavaliers-blue-white.png"
            ),
        ).asSequence()
    ) : PreviewParameterProvider<Good>
}