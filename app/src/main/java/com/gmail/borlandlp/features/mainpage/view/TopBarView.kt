package com.gmail.borlandlp.features.mainpage.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.gmail.borlandlp.features.R

class TopBarView {
    var listener: Listener? = null

    @Composable
    @Preview(showBackground = true)
    fun GetView() {
        ConstraintLayout(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            val (menuBtn, logoBtn, notifyBtn) = createRefs()

            Image(
                painter = painterResource(id = R.drawable.ic_menu_icon),
                contentDescription = "menu btn",
                modifier = Modifier
                    .width(27.dp)
                    .height(27.dp)
                    .constrainAs(menuBtn) {
                        top.linkTo(parent.top)
                        start.linkTo(parent.start)
                    }
                    .clickable {
                        listener?.onSidebarClicked()
                    }
            )

            Image(
                painter = painterResource(id = R.drawable.ic_nike_logo),
                contentDescription = "nike logo",
                modifier = Modifier
                    .width(50.dp)
                    .constrainAs(logoBtn) {
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                        start.linkTo(menuBtn.end)
                        end.linkTo(notifyBtn.start)
                    }
                    .clickable {
                        listener?.onLogoClicked()
                    }
            )

            Image(
                painter = painterResource(id = R.drawable.ic_notify),
                contentDescription = "notify btn",
                modifier = Modifier
                    .width(21.dp)
                    .height(22.dp)
                    .constrainAs(notifyBtn) {
                        top.linkTo(parent.top)
                        end.linkTo(parent.end)
                    }
                    .clickable {
                        listener?.onNotifyClicked()
                    }
            )
        }
    }

    interface Listener {
        fun onSidebarClicked()
        fun onLogoClicked()
        fun onNotifyClicked()
    }
}