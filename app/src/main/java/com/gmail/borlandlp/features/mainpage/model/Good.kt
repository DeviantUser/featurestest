package com.gmail.borlandlp.features.mainpage.model

data class Good (
    var id: String,
    var name: String,
    var inFavorites: Boolean,
    var inCart: Boolean,
    var price: Float,
    val imageUrl: String,
)